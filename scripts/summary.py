import sys
import pandas as pd


def make_summary(files_path):
    result = dict()
    for file in files_path:
        result[file] = pd.read_json(file)["numbers"]
    return pd.DataFrame(result)


if __name__ == '__main__':
    output, *files = sys.argv[1:]
    summary = make_summary(files)
    summary.to_json(output)
