import sys
from pandas import DataFrame


def compute_stats(data):
    return data.describe()


if __name__ == '__main__':
    input_, output = sys.argv[1], sys.argv[2]
    df = DataFrame.from_csv(input_)
    stat = compute_stats(df)
    stat.to_json(output)
