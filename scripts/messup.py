import sys
from pandas import DataFrame


def multiply_data(data, scalar):
    return data.multiply(scalar)


if __name__ == '__main__':
    input_, output = sys.argv[1], sys.argv[2]
    df = DataFrame.from_csv(input_)
    df = multiply_data(df, 2)
    df.to_csv(output)
