import yaml
import sys
from numpy.random import randint, seed
from pandas import DataFrame


def generate_numbers(numbers_count, seed_value=10027):
    seed(seed_value)
    numbers = randint(0, 100, size=numbers_count)
    data = DataFrame(numbers, columns=["numbers"])
    return data


if __name__ == '__main__':
    output = sys.argv[1]
    with open("params.yaml", 'r') as fd:
        params = yaml.safe_load(fd)
    N = params['generator']['N']
    seed_ = params['generator']['seed']

    df = generate_numbers(N, seed_value=seed_)
    df.to_csv(output)
